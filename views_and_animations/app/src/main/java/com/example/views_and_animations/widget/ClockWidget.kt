package com.example.views_and_animations.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import com.example.views_and_animations.data.TimeState
import kotlin.math.cos
import kotlin.math.sin


class ClockView : View {
    private val clockHourList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
    private var currentSecondValue = 0L
    private var myX: Float = 0f
    private var myY: Float = 0f
    private val paint = Paint()
    private val myRect = Rect()

    constructor(context: Context?) : super(context)

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int = 0
    ) : super(context, attrs, defStyleAttr)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val hour = (currentSecondValue / 60 / 60).toInt()
        val minute = ((currentSecondValue - (hour * 60 * 60)) / 60).toInt()
        val second = currentSecondValue - hour * 60 * 60 - minute * 60

        myX = height.toFloat() / 2
        myY = width.toFloat() / 2

        paint.style = Paint.Style.FILL
        paint.setColor(Color.GRAY)
        canvas.drawCircle(myX, myY, clockRadius, paint)

        paint.strokeWidth = clockInnerRadiusStrokeWidth
        paint.style = Paint.Style.STROKE
        paint.setColor(Color.WHITE)
        canvas.drawCircle(myX, myY, clockInnerRadius, paint)

        paint.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            clockHourFontSize,
            resources.displayMetrics
        )
        clockHourList.forEach { value ->
            paint.getTextBounds(value.toString(), 0, value.toString().length, myRect)
            val angle = Math.PI / 6 * (value - 3)
            val valueX: Float = myX + (cos(angle) * clockHourRadius - myRect.width() / 2).toFloat()
            val valueY: Float =
                myY + 30 + (sin(angle) * clockHourRadius - myRect.height() / 2).toFloat()
            canvas.drawText(value.toString(), valueX, valueY, paint)
        }

        paint.color = Color.RED
        drawHandLine(
            canvas,
            ((hour + minute / 60) * 5).toDouble(),
            hourArrowLength,
            hourArrowWidth
        )

        paint.color = Color.CYAN
        drawHandLine(
            canvas,
            minute.toDouble(),
            minuteArrowLength,
            minuteArrowWidth
        )

        paint.color = Color.WHITE
        drawHandLine(
            canvas,
            second.toDouble(),
            secondArrowLength,
            secondArrowWidth
        )
        postInvalidateDelayed(500)
        invalidate()
    }

    private fun setTime(secondValue: Long) {
        if (currentSecondValue != secondValue) {
            currentSecondValue = secondValue
        }
    }

    fun setState(timerState: TimeState) {
        setTime(timerState.time)
    }

    private fun drawHandLine(
        canvas: Canvas,
        moment: Double,
        length: Float,
        width: Float
    ) {
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = width
        val angle: Double = (Math.PI * moment / 30 - Math.PI / 2)
        canvas.drawLine(
            myX,
            myY,
            myX + (cos(angle) * length).toFloat(),
            myY + (sin(angle) * length).toFloat(),
            paint
        )
    }

    companion object {
        const val clockRadius = 250f
        const val clockInnerRadius = clockRadius - 20f
        const val clockInnerRadiusStrokeWidth = 5f
        const val clockHourRadius = 180f
        const val clockHourFontSize = 14f
        const val hourArrowLength = 100f
        const val hourArrowWidth = 15f
        const val minuteArrowLength = 150f
        const val minuteArrowWidth = 10f
        const val secondArrowLength = 180f
        const val secondArrowWidth = 5f
    }
}