package com.example.views_and_animations.data

class TimeState(val time: Long, val isPlayed: Boolean)