package com.example.views_and_animations.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.views_and_animations.R
import com.example.views_and_animations.data.TimeState
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class TimerWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private var mainCoroutineScope = CoroutineScope(Dispatchers.Main)
    private var mainJob: Job = Job()
    private var startButton: Button
    private var resetButton: Button
    private var timeTextView: TextView
    private var timeListenerSet = mutableSetOf<(TimeState) -> Unit>()
    private var timeState: TimeState = TimeState(initSecondValue, false)
        set(value) {
            field = TimeState(if (maxSecondValue < value.time) 1 else value.time, value.isPlayed)
            val calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = field.time * 1000
            val dateFormat = SimpleDateFormat(timeFormat)

            timeTextView.text = dateFormat.format(calendar.time)
            timeListenerSet.forEach { it(field) }
        }

    init {
        val root = inflate(context, R.layout.timer_widget_view, this)
        if (mainJob.isActive)
            mainJob.cancel()
        startButton = root.findViewById(R.id.startButton)
        resetButton = root.findViewById(R.id.resetButton)
        timeTextView = root.findViewById(R.id.timeTextView)
        startButton.setOnClickListener {
            if (timeState.isPlayed) {
                stop()
                startButton.text = resources.getString(R.string.start_button_start_text)
            } else {
                start()
                startButton.text = resources.getString(R.string.start_button_stop_text)

            }
        }
        resetButton.setOnClickListener {
            reset()
        }
        timeState = TimeState(initSecondValue, false)
    }


    fun addListener(listener: (TimeState) -> Unit) {
        timeListenerSet.add(listener)
        listener(timeState)
    }

    fun removeListener(listener: (TimeState) -> Unit) = timeListenerSet.remove(listener)

    private fun start() {
        timeState = TimeState(timeState.time, true)
        if (!mainJob.isActive)
            mainJob = mainCoroutineScope.launch {
                while (true) {
                    delay(customSecondPeriodLength)
                    timeState = TimeState(timeState.time + 1, true)
                }
            }
    }

    fun stop() {
        timeState = TimeState(timeState.time, false)
        if (mainJob.isActive) mainJob.cancel()
    }

    private fun reset() {
        timeState = TimeState(0L, timeState.isPlayed)
    }

    fun currentTime(): Long {
        return timeState.time
    }

    companion object {
        const val initSecondValue = (23 * (60 * 60) + 59 * (60) + 35).toLong()
        const val maxSecondValue = (24 * 60 * 60).toLong()
        const val customSecondPeriodLength = (1000).toLong()
        const val timeFormat = "HH:mm:ss"
    }

}