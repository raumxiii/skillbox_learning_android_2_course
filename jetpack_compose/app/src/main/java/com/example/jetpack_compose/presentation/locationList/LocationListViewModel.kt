package com.example.jetpack_compose.presentation.locationList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.jetpack_compose.data.dto.LocationDto
import kotlinx.coroutines.flow.Flow

class LocationListViewModel : ViewModel() {
    val pagedLocationList: Flow<PagingData<LocationDto>> =
        Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = { LocationPagingSource() })
            .flow.cachedIn(viewModelScope)
}