package com.example.jetpack_compose.presentation.locationList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.jetpack_compose.presentation.composable.ComposableSampleTheme
import com.example.jetpack_compose.presentation.composable.LocationListView

class LocationListFragment : Fragment() {

    companion object {
        fun newInstance() = LocationListFragment()
    }

    private val viewModel: LocationListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {
            setContent {
                ComposableSampleTheme {
                    LocationListView(
                        itemList =
                        viewModel.pagedLocationList.collectAsLazyPagingItems()
                    )
                }
            }
        }
    }
}
