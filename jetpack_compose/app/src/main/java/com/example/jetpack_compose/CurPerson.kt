package com.example.jetpack_compose

import com.example.jetpack_compose.data.dto.PersonDto

object CurPerson {
    private var curPerson: PersonDto = PersonDto(
        id = -1,
        name = "",
        status = "",
        species = "",
        type = "",
        gender = "",
        origin = null,
        location = null,
        episode = listOf(),
        image = "",
        url = "",
        created = ""
    )

    fun setPerson(person: PersonDto) {
        curPerson = person
    }

    fun getPerson(): PersonDto {
        return curPerson
    }
}