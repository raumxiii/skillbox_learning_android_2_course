package com.example.jetpack_compose.presentation.personList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.jetpack_compose.State
import com.example.jetpack_compose.data.dto.PersonDto
import com.example.jetpack_compose.presentation.personList.personRecyclerView.PersonPagingSource
import kotlinx.coroutines.flow.Flow

class PersonListViewModel : ViewModel() {
    val state = MutableLiveData<State>(State.Ready)
    val pagedPersonList: Flow<PagingData<PersonDto>> =
        Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = { PersonPagingSource(state) })
            .flow.cachedIn(viewModelScope)
}