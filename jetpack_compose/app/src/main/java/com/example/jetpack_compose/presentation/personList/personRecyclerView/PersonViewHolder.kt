package com.example.jetpack_compose.presentation.personList.personRecyclerView

import androidx.recyclerview.widget.RecyclerView
import com.example.jetpack_compose.databinding.PersonItemBinding

class PersonViewHolder(val binding: PersonItemBinding) : RecyclerView.ViewHolder(binding.root)