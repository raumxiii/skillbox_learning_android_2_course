package com.example.jetpack_compose.presentation.personList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.jetpack_compose.CurPerson
import com.example.jetpack_compose.R
import com.example.jetpack_compose.State
import com.example.jetpack_compose.data.dto.PersonDto
import com.example.jetpack_compose.databinding.FragmentPersonListBinding
import com.example.jetpack_compose.presentation.personList.personRecyclerView.RecyclerPagedViewAdapter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import androidx.fragment.app.replace

class PersonListFragment : Fragment() {

    companion object {
        fun newInstance() = PersonListFragment()
    }

    private var _binding: FragmentPersonListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: PersonListViewModel by viewModels()
    private val personRecyclerAdapter =
        RecyclerPagedViewAdapter { person -> showPersonDetail(person) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPersonListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recycler.adapter = personRecyclerAdapter

        viewModel.pagedPersonList.onEach {
            personRecyclerAdapter.submitData(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.state.observe(viewLifecycleOwner) {
            binding.recycler.isVisible = it != State.Error
            binding.refreshButton.isVisible = it == State.Error
            binding.loadingProgressBar.isVisible = it == State.Loading
        }
        binding.refreshButton.setOnClickListener {
            refreshList()
        }
    }

    private fun refreshList() {
        personRecyclerAdapter.refresh()
        viewModel.state.postValue(State.Ready)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showPersonDetail(item: PersonDto) {
        CurPerson.setPerson(item)
        parentFragmentManager.commit {
            replace<PersonDetailFragment>(R.id.fragment_container)
        }
    }
}