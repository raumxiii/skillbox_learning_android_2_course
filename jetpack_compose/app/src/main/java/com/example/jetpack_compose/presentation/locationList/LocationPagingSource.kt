package com.example.jetpack_compose.presentation.locationList

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.jetpack_compose.data.RickAndMortyRepository
import com.example.jetpack_compose.data.dto.LocationDto

class LocationPagingSource :
    PagingSource<Int, LocationDto>() {
    private val repository = RickAndMortyRepository()
    override fun getRefreshKey(state: PagingState<Int, LocationDto>): Int? = FIRST_PAGE

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, LocationDto> {
        val page = params.key ?: FIRST_PAGE

        return kotlin.runCatching {
            repository.getLocationList(page = page)
        }.fold(onSuccess = {
            if (it != null)
                LoadResult.Page(
                    data = it,
                    prevKey = null,
                    nextKey = if (it.isEmpty()) null else page + 1
                )
            else {
                Log.e(LOG_TAG, "Load Error: Location List is Empty\n")
                LoadResult.Error(IllegalAccessException())
            }
        },
            onFailure = {
                Log.e(LOG_TAG, "Load Error: ${it.message} cause: ${it.cause}\n")
                LoadResult.Error(it)
            })
    }

    private companion object {
        private const val FIRST_PAGE = 1
        private const val LOG_TAG = "LocationPagingSourceLog"
    }
}