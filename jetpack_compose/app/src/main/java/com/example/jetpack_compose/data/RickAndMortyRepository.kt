package com.example.jetpack_compose.data

import androidx.lifecycle.MutableLiveData
import com.example.jetpack_compose.State
import com.example.jetpack_compose.data.dto.LocationDto
import com.example.jetpack_compose.data.dto.PersonDto
import kotlinx.coroutines.delay

class RickAndMortyRepository {
    suspend fun getPersonList(state: MutableLiveData<State>, page: Int?): List<PersonDto> {
        val curFunctionName = Thread.currentThread().stackTrace[2].methodName
        state.postValue(State.Loading)
        if (TEST_ERROR_PAGE_NUM > 0 && page == TEST_ERROR_PAGE_NUM) throw IllegalStateException(
            String.format(
                TEST_ERROR_MESSAGE,
                curFunctionName
            )
        )
        delay(DELAY_VALUE)

        lateinit var personList: List<PersonDto>
        val response = RetrofitInstance.rickAndMortyDataSource.getPersonList(page)
        if (response.isSuccessful && response.body() != null) {
            personList = response.body()?.personList!!
            state.postValue(State.Ready)
        } else throw IllegalStateException(
            String.format(
                BAD_RESPONSE_ERROR_MESSAGE_FORMAT,
                curFunctionName
            )
        )

        return personList
    }

    suspend fun getLocationList(page: Int?): List<LocationDto> {
        val curFunctionName = Thread.currentThread().stackTrace[2].methodName
        delay(DELAY_VALUE)

        lateinit var locationList: List<LocationDto>
        val response = RetrofitInstance.rickAndMortyDataSource.getLocationList(page)
        if (response.isSuccessful && response.body() != null) {
            locationList = response.body()?.locationList!!
        } else throw IllegalStateException(
            String.format(
                BAD_RESPONSE_ERROR_MESSAGE_FORMAT,
                curFunctionName
            )
        )

        return locationList
    }

    companion object {
        const val TEST_ERROR_PAGE_NUM = 5
        const val TEST_ERROR_MESSAGE = "%1s - User Error"
        const val BAD_RESPONSE_ERROR_MESSAGE_FORMAT = "%1s - Bad Response"
        const val DELAY_VALUE: Long = 1000
    }
}