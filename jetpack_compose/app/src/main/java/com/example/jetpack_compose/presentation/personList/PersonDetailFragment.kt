package com.example.jetpack_compose.presentation.personList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.example.jetpack_compose.CurPerson
import com.example.jetpack_compose.R
import com.example.jetpack_compose.presentation.composable.ComposableSampleTheme
import com.example.jetpack_compose.presentation.composable.PersonDetailView

class PersonDetailFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {
            setContent {
                ComposableSampleTheme {
                    PersonDetailView(person = CurPerson.getPerson(), onClickAction = {
                        returnToListFragment()
                    })
                }
            }
        }
    }

    private fun returnToListFragment() {
        parentFragmentManager.commit {
            replace<PersonListFragment>(R.id.fragment_container)
        }
    }
}