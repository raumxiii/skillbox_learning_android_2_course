package com.example.jetpack_compose.entity

interface ListInfo {
    val count: Int
    val pageCount: Int
    val nextPageUrl: String?
    val prevPageUrl: String?
}