package com.example.jetpack_compose.data.dto

import com.example.jetpack_compose.entity.Origin
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OriginDto(
    @Json(name = "name") override val name: String,
    @Json(name = "url") override val url: String
) : Origin
