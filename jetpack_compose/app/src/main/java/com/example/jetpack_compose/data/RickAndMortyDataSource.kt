package com.example.jetpack_compose.data

import com.example.jetpack_compose.data.dto.LocationListDto
import com.example.jetpack_compose.data.dto.PersonListDto
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

private const val BASE_URL = "https://rickandmortyapi.com/"

interface RickAndMortyDataSource {
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET("api/character/")
    suspend fun getPersonList(
        @Query("page") page: Int?
    ): Response<PersonListDto>

    @GET("api/location/")
    suspend fun getLocationList(
        @Query("page") page: Int?
    ): Response<LocationListDto>
}

object RetrofitInstance {
    val rickAndMortyDataSource =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create<RickAndMortyDataSource>()
}