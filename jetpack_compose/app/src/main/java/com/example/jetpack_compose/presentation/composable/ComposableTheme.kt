package com.example.jetpack_compose.presentation.composable

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color


private val LightColorPalette = lightColors(
    primary = Color.Blue,
    primaryVariant = Color.Gray,
    secondary = Color.LightGray,
    surface = Color.LightGray
)

private val DarkColorPalette = lightColors(
    primary = Color.Blue,
    primaryVariant = Color.Gray,
    secondary = Color.Black,
    surface = Color.DarkGray
)

@Composable
fun ComposableSampleTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        content = content
    )
}