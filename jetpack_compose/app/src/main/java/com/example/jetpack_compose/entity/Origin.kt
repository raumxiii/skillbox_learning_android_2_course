package com.example.jetpack_compose.entity

interface Origin {
    val name: String
    val url: String
}