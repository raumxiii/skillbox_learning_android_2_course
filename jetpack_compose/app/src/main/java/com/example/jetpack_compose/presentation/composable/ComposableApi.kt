package com.example.jetpack_compose.presentation.composable

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import com.example.jetpack_compose.R
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.items
import com.example.jetpack_compose.data.dto.LocationDto
import com.example.jetpack_compose.data.dto.PersonDto
import com.google.accompanist.glide.rememberGlidePainter

private const val NULL_LOCATION_MESSAGE = "Nothing to Show"
private const val RETRY_BUTTON_TEXT = "Retry"

@Composable
fun GlideImageWithPreview(
    dataUrl: String?,
    modifier: Modifier? = null,
    contentDescription: String? = null,
    contentScale: ContentScale = ContentScale.Fit
) {
    println(dataUrl)

    val painter = if (dataUrl == null)
        painterResource(id = R.drawable.ic_launcher_foreground)
    else
        rememberGlidePainter(dataUrl)

    Image(
        painter = painter,
        contentDescription = contentDescription,
        modifier = modifier ?: Modifier,
        alignment = Alignment.Center,
        contentScale = contentScale
    )
}

@Composable
fun LocationListView(itemList: LazyPagingItems<LocationDto>) {
    LazyColumn {
        items(itemList) {
            it?.let { LocationView(location = it) }
                ?: Text(text = NULL_LOCATION_MESSAGE)
        }

        itemList.apply {
            when {
                loadState.refresh is LoadState.Loading -> {
                    item {
                        Box(
                            modifier = Modifier.fillMaxSize()
                        ) {
                            CircularProgressIndicator()
                        }
                    }
                }
                loadState.refresh is LoadState.Error -> {
                    val error = itemList.loadState.refresh as LoadState.Error
                    item {
                        Column(Modifier.fillMaxSize()) {
                            error.error.localizedMessage?.let { Text(text = it) }
                            Button(onClick = { retry() }) { Text(text = RETRY_BUTTON_TEXT) }
                        }
                    }
                }
                loadState.append is LoadState.Loading -> {
                    item {
                        CircularProgressIndicator()
                    }
                }
                loadState.append is LoadState.Error -> {
                    val error = itemList.loadState.append as LoadState.Error
                    item {
                        Column(Modifier.fillMaxSize()) {
                            error.error.localizedMessage?.let { Text(text = it) }
                            Button(onClick = { retry() }) { Text(text = RETRY_BUTTON_TEXT) }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun LocationView(location: LocationDto) {
    Card(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxSize(),
        shape = MaterialTheme.shapes.medium,
        backgroundColor = MaterialTheme.colors.surface,
        border = BorderStroke(1.dp, Color.Gray)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(10.dp)
        ) {
            Column(modifier = Modifier.padding(10.dp)) {
                Text(
                    text = location.name,
                    maxLines = 1,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                )
                Row()
                {
                    Text(
                        text = "Dimension: ",
                        maxLines = 1,
                        fontSize = 15.sp
                    )
                    Text(
                        text = location.dimension.toString(),
                        maxLines = 1,
                        fontWeight = FontWeight.Bold,
                        fontSize = 15.sp
                    )
                }
                Row()
                {
                    Text(
                        text = "Type: ",
                        maxLines = 1,
                        fontSize = 15.sp
                    )
                    Text(
                        text = location.type.toString(),
                        maxLines = 1,
                        fontWeight = FontWeight.Bold,
                        fontSize = 15.sp
                    )
                }
            }
        }
    }
}

@Composable
fun PersonDetailView(person: PersonDto, onClickAction: () -> Unit) {
    val scrollState = rememberScrollState()
    Card(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxSize(),
        shape = MaterialTheme.shapes.medium,
        backgroundColor = MaterialTheme.colors.surface,
        border = BorderStroke(1.dp, Color.Gray)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState)
                .padding(10.dp)
        ) {
            Button(
                onClick = { onClickAction() }
            ) { Text(text = "Back") }
            GlideImageWithPreview(
                dataUrl = person.image, modifier = Modifier
                    .padding(bottom = 15.dp)
                    .align(Alignment.CenterHorizontally)
            )
            Row {
                Text(
                    text = "Name: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.name,
                    fontSize = 15.sp
                )
            }
            Row {
                Text(
                    text = "Status: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.status,
                    fontSize = 15.sp
                )
            }
            Row {
                Text(
                    text = "Type: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.type ?: "",
                    fontSize = 15.sp
                )
            }
            Row {
                Text(
                    text = " Gender: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.gender,
                    fontSize = 15.sp
                )
            }

            Row {
                Text(
                    text = "Species: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.species,
                    fontSize = 15.sp
                )
            }
            Row {
                Text(
                    text = "Created: ",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = person.created,
                    fontSize = 15.sp
                )
            }
            Text(
                text = "Episode:",
                fontSize = 15.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(0.dp, 15.dp, 0.dp, 0.dp)
            )
            person.episode.forEach {
                Text(text = it)
            }
        }
    }
}