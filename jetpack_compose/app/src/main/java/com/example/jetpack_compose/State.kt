package com.example.jetpack_compose

sealed class State {
    object Ready : State()
    object Loading : State()
    object Error : State()
}
