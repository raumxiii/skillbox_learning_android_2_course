package com.example.jetpack_compose.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocationListDto(
    @Json(name = "info") val listInfo: ListInfoDto,
    @Json(name = "results") val locationList: List<LocationDto>
)
