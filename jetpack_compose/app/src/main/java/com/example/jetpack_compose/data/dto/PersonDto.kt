package com.example.jetpack_compose.data.dto

import com.example.jetpack_compose.entity.Person
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonDto(
    @Json(name = "id") override val id: Int,
    @Json(name = "name") override val name: String,
    @Json(name = "status") override val status: String,
    @Json(name = "species") override val species: String,
    @Json(name = "type") override val type: String?,
    @Json(name = "gender") override val gender: String,
    @Json(name = "origin") override val origin: OriginDto?,
    @Json(name = "location") override val location: LocationDto?,
    @Json(name = "episode") override val episode: List<String>,
    @Json(name = "image") override val image: String,
    @Json(name = "url") override val url: String,
    @Json(name = "created") override val created: String
) : Person
