package com.example.dependency_injection.entity

interface BicycleFactory {
    fun build(logo: String, color: Int): Bicycle
}