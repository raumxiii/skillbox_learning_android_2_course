package com.example.dependency_injection

import android.app.Application
import com.example.dependency_injection.data.SimpleBicycleFactory
import com.example.dependency_injection.data.SimpleFrameFactory
import com.example.dependency_injection.data.SimpleSerialProvider
import com.example.dependency_injection.data.SimpleWheelDealer
import com.example.dependency_injection.entity.BicycleFactory
import com.example.dependency_injection.entity.SerialProvider
import com.example.dependency_injection.entity.WheelDealer
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {
    companion object {
        lateinit var component: DaggerComponent
        private val simpleSerialProvider = SimpleSerialProvider()
        private val frameFactory = SimpleFrameFactory(simpleSerialProvider)
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerDaggerComponent
            .builder()
            .daggerModule(DaggerModule(frameFactory))
            .build()

        startKoin {
            modules(module {
                factory<SerialProvider> { SimpleSerialProvider() }
                factory<WheelDealer> { SimpleWheelDealer(get()) }
                factory<BicycleFactory> { SimpleBicycleFactory(frameFactory, get()) }
            })
        }
    }
}