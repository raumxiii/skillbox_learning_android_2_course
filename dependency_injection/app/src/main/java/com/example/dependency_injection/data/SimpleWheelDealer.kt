package com.example.dependency_injection.data

import com.example.dependency_injection.entity.SerialProvider
import com.example.dependency_injection.entity.Wheel
import com.example.dependency_injection.entity.WheelDealer

class SimpleWheelDealer(private val serialProvider: SerialProvider) : WheelDealer {
    override fun getWheel(): Wheel {
        return SimpleWheel(serialProvider.getSerial())
    }
}