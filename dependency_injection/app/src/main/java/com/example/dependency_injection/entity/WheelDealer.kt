package com.example.dependency_injection.entity

import com.example.dependency_injection.entity.Wheel

interface WheelDealer {
    fun getWheel(): Wheel
}