package com.example.dependency_injection.entity

interface Bicycle {
    var wheelFirst: Wheel
    var wheelSecond: Wheel
    var frame: Frame
    var logo: String
}