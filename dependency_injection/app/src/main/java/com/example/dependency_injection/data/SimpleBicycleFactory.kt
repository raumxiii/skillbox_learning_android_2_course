package com.example.dependency_injection.data

import com.example.dependency_injection.entity.Bicycle
import com.example.dependency_injection.entity.BicycleFactory
import com.example.dependency_injection.entity.FrameFactory
import com.example.dependency_injection.entity.WheelDealer
import javax.inject.Inject

class SimpleBicycleFactory(
    private val frameFactory: FrameFactory,
    private val wheelDealer: WheelDealer
) : BicycleFactory {
    override fun build(logo: String, color: Int): Bicycle {
        return SimpleBicycle(
            wheelDealer.getWheel(),
            wheelDealer.getWheel(),
            frameFactory.getFrame(color),
            logo
        )
    }
}