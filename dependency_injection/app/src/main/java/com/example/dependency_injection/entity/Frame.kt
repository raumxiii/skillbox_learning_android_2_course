package com.example.dependency_injection.entity

interface Frame {
    val serialNumber: String
    val color: Int
}