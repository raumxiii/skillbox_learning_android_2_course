package com.example.dependency_injection.data

import com.example.dependency_injection.entity.Wheel

class SimpleWheel(override val serialNumber: String) : Wheel {
}