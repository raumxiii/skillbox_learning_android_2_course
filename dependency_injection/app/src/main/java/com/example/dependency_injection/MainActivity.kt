package com.example.dependency_injection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.dependency_injection.databinding.ActivityMainBinding
import com.example.dependency_injection.entity.BicycleFactory
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var dFactory: BicycleFactory
    private val kFactory: BicycleFactory by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dFactory = App.component.bicycleFactory()

        binding.daggerFactoryButton.setOnClickListener {
            val bicycle = dFactory.build(DAGGER_BICYCLE_LOGO, DAGGER_BICYCLE_COLOR)
            Toast.makeText(
                this, String.format(
                    BICYCLE_TOAST_TEXT_FORMAT,
                    bicycle.frame.serialNumber,
                    bicycle.wheelFirst.serialNumber,
                    bicycle.wheelSecond.serialNumber
                ), Toast.LENGTH_SHORT
            ).show()
        }

        binding.koinFactoryButton.setOnClickListener {
            val bicycle = kFactory.build(KOIN_BICYCLE_LOGO, KOIN_BICYCLE_COLOR)
            Toast.makeText(
                this, String.format(
                    BICYCLE_TOAST_TEXT_FORMAT,
                    bicycle.frame.serialNumber,
                    bicycle.wheelFirst.serialNumber,
                    bicycle.wheelSecond.serialNumber
                ), Toast.LENGTH_SHORT
            ).show()
        }
    }

    companion object {
        private const val DAGGER_BICYCLE_LOGO = "DAGGER_B1"
        private const val DAGGER_BICYCLE_COLOR = 123
        private const val KOIN_BICYCLE_LOGO = "DAGGER_B1"
        private const val KOIN_BICYCLE_COLOR = 333
        private const val BICYCLE_TOAST_TEXT_FORMAT =
            "Bicycle was Created:\nframe serial: %1s, first wheel serial: %2s, second wheel serial: %3s"
    }
}