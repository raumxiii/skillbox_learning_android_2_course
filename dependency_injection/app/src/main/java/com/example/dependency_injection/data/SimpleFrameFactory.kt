package com.example.dependency_injection.data

import com.example.dependency_injection.entity.Frame
import com.example.dependency_injection.entity.FrameFactory
import com.example.dependency_injection.entity.SerialProvider

class SimpleFrameFactory(private val serialProvider: SerialProvider) : FrameFactory {
    override fun getFrame(color: Int): Frame {
        return SimpleFrame(serialProvider.getSerial(), color)
    }
}