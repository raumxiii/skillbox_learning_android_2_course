package com.example.dependency_injection.entity

interface FrameFactory {
    fun getFrame(color: Int): Frame
}