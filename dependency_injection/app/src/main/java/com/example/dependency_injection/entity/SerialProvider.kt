package com.example.dependency_injection.entity

interface SerialProvider {
    fun getSerial(): String
}