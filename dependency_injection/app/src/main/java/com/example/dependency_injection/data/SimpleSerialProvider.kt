package com.example.dependency_injection.data

import com.example.dependency_injection.entity.SerialProvider
import kotlin.random.Random

class SimpleSerialProvider : SerialProvider {
    override fun getSerial(): String {
        var serial: String = ""
        repeat(Random.nextInt(1, 10)) { serial += Char(Random.nextInt(48, 122)) }
        return serial
    }
}