package com.example.dependency_injection.data

import com.example.dependency_injection.entity.Bicycle
import com.example.dependency_injection.entity.Frame
import com.example.dependency_injection.entity.Wheel

class SimpleBicycle(
    override var wheelFirst: Wheel,
    override var wheelSecond: Wheel,
    override var frame: Frame,
    override var logo: String
) : Bicycle