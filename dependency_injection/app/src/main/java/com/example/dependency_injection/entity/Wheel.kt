package com.example.dependency_injection.entity

interface Wheel {
    val serialNumber: String
}