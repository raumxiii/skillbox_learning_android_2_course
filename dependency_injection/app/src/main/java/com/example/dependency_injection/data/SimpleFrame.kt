package com.example.dependency_injection.data

import com.example.dependency_injection.entity.Frame

class SimpleFrame(override val serialNumber: String, override val color: Int) : Frame {
}