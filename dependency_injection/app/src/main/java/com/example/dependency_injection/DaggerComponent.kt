package com.example.dependency_injection

import com.example.dependency_injection.data.SimpleBicycleFactory
import com.example.dependency_injection.data.SimpleSerialProvider
import com.example.dependency_injection.data.SimpleWheelDealer
import com.example.dependency_injection.entity.BicycleFactory
import com.example.dependency_injection.entity.FrameFactory
import com.example.dependency_injection.entity.SerialProvider
import com.example.dependency_injection.entity.WheelDealer
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Component(
    modules = [DaggerModule::class]
)

interface DaggerComponent {
    fun bicycleFactory(): BicycleFactory
}

@Module
class DaggerModule(private val frameFactory: FrameFactory) {
    @Provides
    @Singleton
    fun serialProvider(): SerialProvider {
        return SimpleSerialProvider()
    }

    @Provides
    @Singleton
    fun wheelDealer(): WheelDealer = SimpleWheelDealer(serialProvider())

    @Provides
    fun bicycleFactory(): BicycleFactory =
        SimpleBicycleFactory(frameFactory, wheelDealer())

}