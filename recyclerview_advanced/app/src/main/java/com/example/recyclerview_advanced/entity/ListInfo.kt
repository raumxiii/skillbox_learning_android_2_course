package com.example.recyclerview_advanced.entity

interface ListInfo {
    val count: Int
    val pageCount: Int
    val nextPageUrl: String
    val prevPageUrl: String?
}