package com.example.recyclerview_advanced.data.dto

import com.example.recyclerview_advanced.entity.Location
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocationDto(
    @Json(name = "name") override val name: String,
    @Json(name = "url") override val url: String
) : Location