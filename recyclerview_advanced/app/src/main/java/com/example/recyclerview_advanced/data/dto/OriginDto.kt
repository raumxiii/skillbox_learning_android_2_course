package com.example.recyclerview_advanced.data.dto

import com.example.recyclerview_advanced.entity.Origin
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OriginDto(
    @Json(name = "name") override val name: String,
    @Json(name = "url") override val url: String
) : Origin
