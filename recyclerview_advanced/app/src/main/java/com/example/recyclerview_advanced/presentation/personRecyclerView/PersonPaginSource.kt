package com.example.recyclerview_advanced.presentation.personRecyclerView

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.recyclerview_advanced.State
import com.example.recyclerview_advanced.data.dto.PersonDto
import com.example.recyclerview_advanced.data.RickAndMortyRepository

class PersonPagingSource(private val state: MutableLiveData<State>) :
    PagingSource<Int, PersonDto>() {
    private val repository = RickAndMortyRepository()
    override fun getRefreshKey(state: PagingState<Int, PersonDto>): Int? = FIRST_PAGE

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PersonDto> {
        val page = params.key ?: FIRST_PAGE

        return kotlin.runCatching {
            repository.getPersonList(state, page = page)
        }.fold(onSuccess = {
            if (it != null)
                LoadResult.Page(
                    data = it,
                    prevKey = null,
                    nextKey = if (it.isEmpty()) null else page + 1
                )
            else {
                Log.e(LOG_TAG, "Load Error: Person List is Empty\n")
                state.postValue(State.Error)
                LoadResult.Error(IllegalAccessException())
            }
        },
            onFailure = {
                Log.e(LOG_TAG, "Load Error: ${it.message} cause: ${it.cause}\n")
                state.postValue(State.Error)
                LoadResult.Error(it)
            })
    }

    private companion object {
        private const val FIRST_PAGE = 1
        private const val LOG_TAG = "PersonPagingSourceLog"
    }
}