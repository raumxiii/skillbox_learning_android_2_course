package com.example.recyclerview_advanced.presentation.personRecyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.example.recyclerview_advanced.data.dto.PersonDto
import com.example.recyclerview_advanced.databinding.PersonItemBinding

class RecyclerPagedViewAdapter :
    PagingDataAdapter<PersonDto, PersonViewHolder>(DiffUtilCallback()) {
    class DiffUtilCallback : DiffUtil.ItemCallback<PersonDto>() {
        override fun areItemsTheSame(oldItem: PersonDto, newItem: PersonDto): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: PersonDto, newItem: PersonDto): Boolean =
            oldItem == newItem
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val item = getItem(position)
        with(holder.binding) {
            nameTextView.text = String.format(NAME_FORMAT_STRING, item?.name)
            statusTextView.text =
                String.format(STATUS_FORMAT_STRING, item?.status, item?.species)
            item?.location?.let {
                locationTextView.text =
                    String.format(LOCATION_FORMAT_STRING, item.location.name)
            }

            item?.let {
                Glide
                    .with(photoImageView.context)
                    .load(it.image)
                    .into(photoImageView)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        return PersonViewHolder(
            PersonItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    companion object {
        const val NAME_FORMAT_STRING = "Name: %1s"
        const val STATUS_FORMAT_STRING = "Status: %1s - %2s"
        const val LOCATION_FORMAT_STRING = "Last known location: %1s"
    }
}