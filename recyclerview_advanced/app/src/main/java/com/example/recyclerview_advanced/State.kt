package com.example.recyclerview_advanced

sealed class State {
    object Ready : State()
    object Loading : State()
    object Error : State()
}
