package com.example.recyclerview_advanced.presentation.personRecyclerView

import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_advanced.databinding.PersonItemBinding

class PersonViewHolder(val binding: PersonItemBinding) : RecyclerView.ViewHolder(binding.root)