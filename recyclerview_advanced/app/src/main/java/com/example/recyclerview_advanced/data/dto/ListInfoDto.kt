package com.example.recyclerview_advanced.data.dto

import com.example.recyclerview_advanced.entity.ListInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ListInfoDto(
    @Json(name = "count") override val count: Int,
    @Json(name = "pages") override val pageCount: Int,
    @Json(name = "next") override val nextPageUrl: String,
    @Json(name = "prev") override val prevPageUrl: String?
):ListInfo
