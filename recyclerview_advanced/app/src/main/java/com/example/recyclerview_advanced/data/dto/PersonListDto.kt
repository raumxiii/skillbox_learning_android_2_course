package com.example.recyclerview_advanced.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PersonListDto(
    @Json(name = "info") val listInfo: ListInfoDto,
    @Json(name = "results") val photoList: List<PersonDto>
)
