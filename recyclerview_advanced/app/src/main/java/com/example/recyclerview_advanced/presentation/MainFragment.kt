package com.example.recyclerview_advanced.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.recyclerview_advanced.State
import com.example.recyclerview_advanced.databinding.FragmentMainBinding
import com.example.recyclerview_advanced.presentation.personRecyclerView.RecyclerPagedViewAdapter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels()
    private val personRecyclerAdapter = RecyclerPagedViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recycler.adapter = personRecyclerAdapter

        viewModel.pagedPersonList.onEach {
            personRecyclerAdapter.submitData(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.state.observe(viewLifecycleOwner) {
            binding.recycler.isVisible = it != State.Error
            binding.refreshButton.isVisible = it == State.Error
            binding.loadingProgressBar.isVisible = it == State.Loading
        }
        binding.refreshButton.setOnClickListener {
            refreshList()
        }
    }

    private fun refreshList() {
        personRecyclerAdapter.refresh()
        viewModel.state.postValue(State.Ready)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}