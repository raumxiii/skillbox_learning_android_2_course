package com.example.recyclerview_advanced.data

import androidx.lifecycle.MutableLiveData
import com.example.recyclerview_advanced.State
import com.example.recyclerview_advanced.data.dto.PersonDto
import kotlinx.coroutines.delay

class RickAndMortyRepository{
    suspend fun getPersonList(state: MutableLiveData<State>, page: Int?): List<PersonDto> {
        state.postValue(State.Loading)
        if (TEST_ERROR_PAGE_NUM > 0 && page == TEST_ERROR_PAGE_NUM) throw IllegalStateException(
            TEST_ERROR_MESSAGE
        )
        delay(DELAY_VALUE)

        lateinit var personList: List<PersonDto>
        val response = RetrofitInstance.rickAndMortyDataSource.getPersonList(page)
        if (response.isSuccessful && response.body() != null) {
            personList = response.body()?.photoList!!
            state.postValue(State.Ready)
        } else throw IllegalStateException(BAD_RESPONSE_ERROR_MESSAGE)

        return personList
    }

    companion object {
        const val TEST_ERROR_PAGE_NUM = 5
        const val TEST_ERROR_MESSAGE = "User Error"
        const val BAD_RESPONSE_ERROR_MESSAGE = "Bad Response"
        const val DELAY_VALUE: Long = 1000
    }
}